\section{NDN-RIOT Design}
\label{sec:design}

\subsection{Objectives}

The main objective for NDN-RIOT implementation is to support the core NDN forwarding operations with minimum computing resources and provide compatibility with the current protocol specification~\cite{ndn-packet-spec}.
We initially target devices with 10s of KB of RAM (for storing runtime data), 100s of KB of flash memory (for storing binary executable code), and low-power CPU running at a frequency of less than 100 MHz.
This requires NDN-RIOT to implement simplified versions of the underlying data structures (PIT, FIB, CS) to fit within the constrained parameters, supporting most of the core requirements for the forwarding mechanisms.
The second but not less important objective is to facilitate development of secure NDN-based applications by providing a high-level API with inherent support for data-centric security primitives.


\subsection{Software Architecture}

\begin{figure}[!t]
\centering
\includegraphics[width=0.85\columnwidth, keepaspectratio=true]{figures/arch.pdf}
\caption{Software architecture of NDN-RIOT}
\label{fig:arch}
\end{figure}

RIOT-OS uses a micro-kernel architecture where the network protocol modules (e.g., IPv6 and UDP) are implemented as kernel threads,%
\footnote{RIOT-OS does not support virtual memory so there is no difference between processes and threads. Moreover, it does not provide separation of user-space and kernel-space, which gives kernel threads and application threads the same level of execution privilege.}
and packet passing across layers is achieved through IPC between different modules.
The NDN-RIOT stack is implemented in the same fashion, as illustrated in Fig.~\ref{fig:arch}.
When the application wants to send an NDN packet, it passes the packet to the NDN thread in an IPC call;
the NDN thread processes the packet and then passes it to the network device (NIC) driver thread for transmission.
When the NIC driver receives a Layer-2 frame that contains an NDN packet, it passes the packet to the NDN thread, which then notifies the applications and/or further propagates the packet to remote NDN-RIOT nodes.


\subsection{Packet Encoding and Decoding}
\label{sec:encoding}

In order to support constrained devices with extremely limited memory resources, we carefully designed the packet encoding and decoding routines to minimize the number of copies of the NDN packets in memory.
An important design decision we made is to store the packet in the wire format form throughout its lifetime in the system and access the underlying elements (e.g., access the name field to decide where to forward the interest or access the signature field during the data packet authentication) through on-demand TLV packet parsing.
This way we saved memory and CPU cycles by not using the deserialized packet, paying the prices of additional CPU processing every time a packet field is accessed.
Fortunately, as we demonstrate in Section~\ref{sec:eval}, parsing the TLV-formatted NDN packet is usually very efficient.

To simplify memory management and ownership tracking, we implemented a lightweight \emph{shared memory block} structure (inspired by the shared pointer mechanism in C++11) for storing the TLV-encoded NDN name, Interest, and Data.
It allows us to maximize the sharing of common data across system modules while avoiding memory leaks due to programming errors.


\subsection{Data Authentication}

Data security is one of the key requirements for IoT applications.
However, because of the limited CPU and memory resources, constrained IoT devices have limitations on which cryptographic operations they can support.
Nevertheless, with the help of the hash API in RIOT-OS kernel and a third-party \emph{micro-ecc} library~\cite{micro-ecc}, we were able to implement three practical data authentication methods that can be used by IoT applications: AES-CCM~\cite{ccm}, HMAC~\cite{hmac}, and ECDSA~\cite{ecdsa}.
Note that AES-CCM is an authenticated encryption algorithm, which provides both data authenticity and secrecy.
Our implementation uses the standard \texttt{secp256r1} curve~\cite{sec2-v2} for ECDSA with a signature length of 64 bytes.
RSA signature algorithm is not supported due to its prohibitive computation cost.

One particular challenge we faced when implementing ECDSA support was that a lot of constrained IoT devices lack the hardware entropy source for generating cryptographically secure random numbers, which is a critical step in the ECDSA signing process.
Using a pseudo-random number generator (PRNG) without a strong source of entropy significantly impairs the strength of the ECDSA signatures.
As a current solution we adopted the use of deterministic ECDSA signing~\cite{rfc6979}, which does not use true random numbers when generating the signatures.
However, creating ECDSA key pairs still requires cryptographically strong random numbers. One way to achieve this is to let the applications on RIOT-OS use ECDSA key pairs created on other platforms and transported to the IoT devices. 
The detailed mechanism of exporting and configuring ECDSA keys is under development and will be described in NDN-RIOT documentation.

\subsection{Packet Forwarding}

To support the NDN packet forwarding logic, NDN-RIOT includes the simplified versions of PIT, FIB, and CS data structures.
In order to minimize memory overhead, all three data structures are implemented as simple linked lists, as we expect low numbers of entries (under 100) on constrained devices.
The current packet processing does not support Interest selectors and all lookups are performed only using Interest and Data names.
The implementation also does not support retrieval of data using full names including the implicit digest component, as IoT applications are primarily used for retrieving data samples and processing commands, which typically do not require the use of implicit digest.

The PIT uses the exact match for the Interest name and any prefix match for the data name (i.e., the Data packet will match any PIT entry with name that is shorter or the same as the data name).
The simplified PIT entries record only incoming faces for the Interests.
During forwarding, PIT state is used to prevent potential loops without using the nonce mechanism.

The FIB table implements the longest-prefix match using Interest names and stores unranked arrays of faces, which can be configured either through static configuration or using a simple IPC-based mechanism at run time.
The latter is currently available only for local application prefix registration.
Propagation of the routing information (proactive or on-demand) to remote nodes, especially over wireless mesh networks, is left for our future work.
Additional IPC-based mechanism for dynamic FIB configuration is also a part of our future plan.

The CS uses a pre-configured maximum size (current compile-time adjustable default is 24~KB) and implements a simple FIFO cache eviction policy.
It benefits from the shared memory block design described in Section~\ref{sec:encoding} and stores the shared pointer to the cached Data packet rather than making a deep copy.

We also implemented a simplified version of the forwarding strategy framework described in the NFD developer guide~\cite{nfd-guide}.
The forwarding strategy inserts custom operations using callback functions at three critical points in the NDN packet processing pipeline: \textit{after an Interest packet is received}, \textit{before a PIT entry is satisfied by a received Data packet}, and \textit{before an expired PIT entry is removed}.
The strategy callback functions have full access to the PIT, FIB, and CS tables and can make forwarding decisions such as (re)transmitting a packet to a specific face or drop the current packet based on the logic of the strategy.
When NDN-RIOT application is deployed, one of the currently available (\textit{best-route unicast} and \textit{multicast}) or a custom-defined strategy can be selected to handle forwarding of all Interests.
Unlike full-featured NFD strategy framework, NDN-RIOT supports only one strategy at a time, which we expect to be sufficient for the expected uses of the platform.

\subsection{Layer-2 Communication}

RIOT-OS currently supports two types of L2 protocols: Ethernet (used by the emulator) and IEEE 802.15.4 (on real devices).
When sending NDN packets over Ethernet, the network device driver sets the destination MAC address to the broadcast address (FF:FF:FF:FF:FF:FF).
The Ethernet header also carries the IEEE 802 protocol number for NDN so that the packet receiver can detect the packet type and dispatch the packet to the NDN thread.
When operating over IEEE 802.15.4 links, the devices tune into the pre-selected wireless channel (identified by the channel ID number, e.g., 26) and Personal Area Network (PAN) (identified by the PAN ID, e.g., 0x23) and use broadcast destination address (FF:FF).
% in order to send packets to and receive packets from each other.
% Our implementation uses the default channel (26) and PAN ID (0x23) configuration in the RIOT-OS kernel.
% The IEEE 802.15.4 packets are always sent to the broadcast address (FF:FF).

Most constrained networks have a very limited MTU size, typically less than 100 bytes.
Although IoT applications optimized for constrained environments should try to avoid using big packets, it is too restrictive to require all applications to always send NDN packets that can fit into the network MTU.
Therefore, we also designed a lightweight hop-by-hop L2 fragmentation and reassembly mechanism for NDN-RIOT.
% , which closely resembles the IP fragmentation design.
%
\begin{figure}[!t]
\centering
\includegraphics[width=0.85\columnwidth, keepaspectratio=true]{figures/l2frag.pdf}
\caption{L2 fragmentation header in NDN-RIOT}
\label{fig:frag-header}
\end{figure}
%
Fig.~\ref{fig:frag-header} shows the format of the 3-byte fragmentation header that is prepended to every fragment of an NDN packet if it needs L2 fragmentation.
The header format is optimized for constrained environments by packing all information into 24 bits.
The first bit of the header is set to 1 to indicate the packet is fragmented.
Unfragmented packet will start with the type code for Interest (5) or Data (6) packet, whose highest-order bit is always 0.
The More-Fragment (MF) bit indicates whether the current packet is the last fragment.
The sequence number (SEQ) and identification fields provide ordering of the fragments, which are used by the receiver to reassemble the original NDN packet.

% The packet fragmentation and reassembly process is similar to that in IP.
% When sending a large packet that exceeds the underlying MTU, the sender will chop the packet into multiple fragments and prepend the fragmentation header (with the same ID) in the beginning.
% The receiver will maintain a soft state of the received fragments and reassemble the NDN packet based on the sequence number and identification in the fragmentation header, once all the fragments of that packet have been successfully received.

\subsection{Application Interface}
\label{sec:api}

We implemented a set of high-level application interface that abstracts away the internal communication mechanism between the application thread and the NDN stack.
Those APIs provide asynchronous communication model that is widely adopted in existing NDN client libraries such as NDN.JS~\cite{ndn-js} and ndn-cxx~\cite{ndn-cxx}.
Under this model, an application runs an event loop that dispatches I/O events (e.g., packet received, timer expired, etc.) on a single thread and invokes the corresponding callbacks to handle those events.
The application may also create multiple run loops in different threads to schedule different tasks.
Table~\ref{tab:api} shows the core APIs that are frequently used in NDN applications.

\begin{table*}[!t]
\centering
% increase table row spacing, adjust to taste
\renewcommand{\arraystretch}{1.2}
% if using array.sty, it might be a good idea to tweak the value of
% \extrarowheight as needed to properly center the text within the cells
\caption{Application interface in NDN-RIOT}
\label{tab:api}
\centering
% Some packages, such as MDW tools, offer better commands for making tables
% than the plain LaTeX2e tabular which is used here.
\begin{tabular}{|l|p{5.2in}|}
\hline
\textbf{Function} & \textbf{Description}\\
\hline
\texttt{ndn\_app\_create} & Create a handle for the NDN application and initialize resources\\
\hline
\texttt{ndn\_app\_run} & Start the application's run-loop and block until the application terminates\\
\hline
\texttt{ndn\_app\_destroy} & Release the application's handle and associated resources\\
\hline
\texttt{ndn\_app\_schedule} & Schedule a callback function to be executed after the specified time interval\\
\hline
\texttt{ndn\_app\_express\_interest} & Send an Interest and register the callbacks for processing the retrieved Data and the Interest timeout event\\
\hline
\texttt{ndn\_app\_register\_prefix} & Register the prefix in the local FIB and the callback for processing the received Interests that match the prefix\\
\hline
\texttt{ndn\_app\_put\_data} & Send a Data packet to the NDN thread to consume previously received Interests% (unsolicited Data will be discarded)
\\\hline
\end{tabular}
\end{table*}

\lstset{language=C,
frame=single,
caption={Skeleton NDN consumer app for RIOT-OS},
captionpos=b,
label=list:consumer,
basicstyle=\footnotesize\ttfamily,
morekeywords={uint8\_t},
%belowcaptionskip=-10pt,
}
\begin{lstlisting}[float,linewidth=\columnwidth]
static ndn_app_t* handle = NULL;

static int on_data(ndn_block_t* interest,
                   ndn_block_t* data) {
  ndn_block_t content;
  ndn_data_get_content(data, &content);
  // do something with content...
  return NDN_APP_STOP;
}

static int on_timeout(ndn_block_t* interest) {
  ndn_block_t name;
  ndn_interest_get_name(interest, &name);
  ndn_name_print(&name);
  return NDN_APP_STOP;
}

static int send_interest(void* context) {
  const char* uri = (const char*)context;
  ndn_shared_block_t* sn =
    ndn_name_from_uri(uri, strlen(uri));
  ndn_app_express_interest(handle, &sn->block,
                           NULL, 4000,
                           on_data, on_timeout);
  ndn_shared_block_release(sn);
  return NDN_APP_CONTINUE;
}

void run_consumer(const char* uri) {
  handle = ndn_app_create();
  ndn_app_schedule(handle, send_interest,
                   (void*)uri, 1000000);
  ndn_app_run(handle);
  ndn_app_destroy(handle);
}
\end{lstlisting}

\lstset{language=C,
frame=single,
caption={Skeleton NDN producer app for RIOT-OS},
captionpos=b,
label=list:producer,
basicstyle=\footnotesize\ttfamily,
morekeywords={uint8\_t},
%belowcaptionskip=-10pt,
}
\begin{lstlisting}[float,linewidth=\columnwidth]
static ndn_app_t* handle = NULL;

static int on_interest(ndn_block_t* interest) {
  ndn_block_t in;
  ndn_interest_get_name(interest, &in);
  ndn_shared_block_t* sdn =
    ndn_name_append_uint8(&in, 0);
  ndn_metainfo_t meta = {NDN_CONTENT_TYPE_BLOB, -1};
  uint8_t buf[20] = {0x23};
  ndn_block_t content = {buf, sizeof(buf)};
  ndn_shared_block_t* sd =
    ndn_data_create(&sdn->block, &meta, &content,
                    NDN_SIG_TYPE_ECDSA_SHA256, NULL,
                    ecc_key, sizeof(ecc_key));
  ndn_shared_block_release(sdn);
  ndn_app_put_data(handle, sd);
  return NDN_APP_CONTINUE;
}

void run_producer(const char* prefix) {
  handle = ndn_app_create();
  ndn_shared_block_t* sp =
    ndn_name_from_uri(prefix, strlen(prefix));
  ndn_app_register_prefix(handle, sp, on_interest)
  ndn_app_run(handle);
  ndn_app_destroy(handle);
}
\end{lstlisting}

We illustrate the usage of the APIs in Listings~\ref{list:consumer} and~\ref{list:producer}, which show the  code of skeleton consumer and producer applications, respectively.
Due to space limit, we omit all the error checking code and the main function that starts the app.
The complete source code can be found at~\cite{source-code}.


% In the consumer example, the \texttt{run\_consumer} function schedules the \texttt{send\_interest} callback to be executed one second after the application starts.
% The callback then sends an Interest packet with the user-specified name.
% The application terminates when either a Data packet is received, or the Interest times out.
% In the producer example, the \texttt{run\_producer} function registers the user-specified prefix and enters the run loop.
% The \texttt{on\_interest} callback is executed when an Interest is received with a name under that prefix.
% The callback will append a single-byte component to the received Interest name and use that to name the returning Data packet which has 20-byte content and is signed with an ECDSA key.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "ndn-riot"
%%% End:

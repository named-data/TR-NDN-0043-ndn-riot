FROM ubuntu:16.04
MAINTAINER Alexander Afanasyev

RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get -y install git build-essential python
RUN apt-get -y install python-setuptools python-dev
RUN easy_install pip
RUN pip install sphinx sphinxcontrib-bibtex
RUN (git clone https://gitlab.com/git-latexdiff/git-latexdiff.git && cd git-latexdiff && make install-bin)
RUN apt-get install -y texlive-latex-recommended
RUN apt-get install -y texlive-fonts-extra
RUN apt-get install -y texlive-formats-extra
RUN apt-get install -y texlive-bibtex-extra
RUN apt-get install -y texlive-generic-extra
RUN apt-get install -y texlive-extra-utils
RUN apt-get install -y latexdiff

# docker build -t registry.gitlab.com/named-data/tr-ndn-0043-ndn-riot .
# docker push registry.gitlab.com/named-data/tr-ndn-0043-ndn-riot
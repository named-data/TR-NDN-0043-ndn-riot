File: bin/samr21-xpro/ndn.a(fib.o)
Symbol table '.symtab' contains 385 entries:
   Num:    Value  Size Type    Bind   Vis      Ndx Name
     7: 00000001    92 FUNC    LOCAL  DEFAULT  118 _fib_entry_add_face
   376: 00000001   272 FUNC    GLOBAL DEFAULT  120 ndn_fib_add
   383: 00000001    60 FUNC    GLOBAL DEFAULT  122 ndn_fib_lookup
   384: 00000001    12 FUNC    GLOBAL DEFAULT  124 ndn_fib_init
File: bin/samr21-xpro/ndn.a(cs.o)
Symbol table '.symtab' contains 362 entries:
   Num:    Value  Size Type    Bind   Vis      Ndx Name
   350: 00000001   180 FUNC    GLOBAL DEFAULT  110 ndn_cs_add
   357: 00000001    88 FUNC    GLOBAL DEFAULT  112 ndn_cs_match
   361: 00000001    20 FUNC    GLOBAL DEFAULT  114 ndn_cs_init
File: bin/samr21-xpro/ndn.a(pit.o)
Symbol table '.symtab' contains 402 entries:
   Num:    Value  Size Type    Bind   Vis      Ndx Name
   380: 00000001   372 FUNC    GLOBAL DEFAULT  119 ndn_pit_add
   393: 00000001   100 FUNC    GLOBAL DEFAULT  121 _ndn_pit_release
   395: 00000001   120 FUNC    GLOBAL DEFAULT  123 ndn_pit_timeout
   397: 00000001   268 FUNC    GLOBAL DEFAULT  125 ndn_pit_match_data
   401: 00000001    12 FUNC    GLOBAL DEFAULT  127 ndn_pit_init
File: bin/samr21-xpro/ndn.a(ndn.o)
Symbol table '.symtab' contains 429 entries:
   Num:    Value  Size Type    Bind   Vis      Ndx Name
     7: 00000001    48 FUNC    LOCAL  DEFAULT  123 _process_data.isra.0
    11: 00000001   216 FUNC    LOCAL  DEFAULT  125 _process_interest
    15: 00000001   420 FUNC    LOCAL  DEFAULT  127 _event_loop
   421: 00000001    84 FUNC    GLOBAL DEFAULT  129 ndn_init
File: bin/samr21-xpro/ndn.a(face_table.o)
Symbol table '.symtab' contains 347 entries:
   Num:    Value  Size Type    Bind   Vis      Ndx Name
   338: 00000001    24 FUNC    GLOBAL DEFAULT  105 ndn_face_table_size
   339: 00000001    28 FUNC    GLOBAL DEFAULT  107 ndn_face_table_find
   340: 00000001    68 FUNC    GLOBAL DEFAULT  109 ndn_face_table_add
   342: 00000001    96 FUNC    GLOBAL DEFAULT  111 ndn_face_table_remove
   346: 00000001    12 FUNC    GLOBAL DEFAULT  113 ndn_face_table_init
File: bin/samr21-xpro/ndn.a(app.o)
Symbol table '.symtab' contains 433 entries:
   Num:    Value  Size Type    Bind   Vis      Ndx Name
     7: 00000001    58 FUNC    LOCAL  DEFAULT  122 _add_consumer_cb_entry.is
   402: 00000001   112 FUNC    GLOBAL DEFAULT  124 ndn_app_create
   408: 00000001   612 FUNC    GLOBAL DEFAULT  126 ndn_app_run
   418: 00000001   280 FUNC    GLOBAL DEFAULT  128 ndn_app_destroy
   420: 00000001    96 FUNC    GLOBAL DEFAULT  130 ndn_app_schedule
   422: 00000001   160 FUNC    GLOBAL DEFAULT  132 ndn_app_express_interest
   425: 00000001   160 FUNC    GLOBAL DEFAULT  134 ndn_app_express_interest2
   427: 00000001   180 FUNC    GLOBAL DEFAULT  136 ndn_app_register_prefix
   428: 00000001    76 FUNC    GLOBAL DEFAULT  138 ndn_app_register_prefix2
   432: 00000001    60 FUNC    GLOBAL DEFAULT  140 ndn_app_put_data
File: bin/samr21-xpro/ndn.a(netif.o)
Symbol table '.symtab' contains 416 entries:
   Num:    Value  Size Type    Bind   Vis      Ndx Name
     7: 00000001    74 FUNC    LOCAL  DEFAULT  127 _ndn_netif_send_packet
   401: 00000001   164 FUNC    GLOBAL DEFAULT  129 ndn_netif_auto_add
   409: 00000001   224 FUNC    GLOBAL DEFAULT  131 ndn_netif_send
File: bin/samr21-xpro/ndn.a(l2.o)
Symbol table '.symtab' contains 413 entries:
   Num:    Value  Size Type    Bind   Vis      Ndx Name
     7: 00000001    10 FUNC    LOCAL  DEFAULT  123 gnrc_pktbuf_release
    10: 00000001   112 FUNC    LOCAL  DEFAULT  125 _release_l2_frag_entry
   400: 00000001    56 FUNC    GLOBAL DEFAULT  127 ndn_l2_frag_build_hdr
   402: 00000001   536 FUNC    GLOBAL DEFAULT  129 ndn_l2_frag_receive
   411: 00000001    36 FUNC    GLOBAL DEFAULT  131 ndn_l2_frag_timeout
   412: 00000001    12 FUNC    GLOBAL DEFAULT  133 ndn_l2_init

File: bin/iotlab-m3/ndn_encoding.a(interest.o)
Symbol table '.symtab' contains 217 entries:
   Num:    Value  Size Type    Bind   Vis      Ndx Name
   197: 00000001   192 FUNC    GLOBAL DEFAULT   60 ndn_interest_create
   209: 00000001   180 FUNC    GLOBAL DEFAULT   62 ndn_interest_create2
   212: 00000001    94 FUNC    GLOBAL DEFAULT   64 ndn_interest_get_name
   214: 00000001   168 FUNC    GLOBAL DEFAULT   66 ndn_interest_get_nonce
   216: 00000001   192 FUNC    GLOBAL DEFAULT   68 ndn_interest_get_lifetime
File: bin/iotlab-m3/ndn_encoding.a(name.o)
Symbol table '.symtab' contains 237 entries:
   Num:    Value  Size Type    Bind   Vis      Ndx Name
     7: 00000001    74 FUNC    LOCAL  DEFAULT   57 _ndn_name_length
   212: 00000001    64 FUNC    GLOBAL DEFAULT   59 ndn_name_component_compar
   214: 00000001   100 FUNC    GLOBAL DEFAULT   61 ndn_name_component_wire_e
   217: 00000001    86 FUNC    GLOBAL DEFAULT   63 ndn_name_compare
   218: 00000001    46 FUNC    GLOBAL DEFAULT   65 ndn_name_get_component
   219: 00000001    22 FUNC    GLOBAL DEFAULT   66 ndn_name_total_length
   220: 00000001   118 FUNC    GLOBAL DEFAULT   68 ndn_name_wire_encode
   221: 00000001   408 FUNC    GLOBAL DEFAULT   70 ndn_name_from_uri
   228: 00000001   232 FUNC    GLOBAL DEFAULT   72 ndn_name_append
   230: 00000001   124 FUNC    GLOBAL DEFAULT   74 ndn_name_get_size_from_bl
   231: 00000001   164 FUNC    GLOBAL DEFAULT   76 ndn_name_get_component_fr
   232: 00000001   166 FUNC    GLOBAL DEFAULT   78 ndn_name_compare_block
   233: 00000001   220 FUNC    GLOBAL DEFAULT   80 ndn_name_print
File: bin/iotlab-m3/ndn_encoding.a(block.o)
Symbol table '.symtab' contains 204 entries:
   Num:    Value  Size Type    Bind   Vis      Ndx Name
   194: 00000001    82 FUNC    GLOBAL DEFAULT   57 ndn_block_get_var_number
   195: 00000001    72 FUNC    GLOBAL DEFAULT   58 ndn_block_put_var_number
   196: 00000001    20 FUNC    GLOBAL DEFAULT   59 ndn_block_var_number_leng
   197: 00000001    22 FUNC    GLOBAL DEFAULT   60 ndn_block_total_length
   198: 00000001    20 FUNC    GLOBAL DEFAULT   62 ndn_block_integer_length
   199: 00000001    64 FUNC    GLOBAL DEFAULT   63 ndn_block_put_integer
   200: 00000001    66 FUNC    GLOBAL DEFAULT   64 ndn_block_get_integer
   201: 00000001    24 FUNC    GLOBAL DEFAULT   65 ndn_block_create_packet
   203: 00000001    80 FUNC    GLOBAL DEFAULT   67 ndn_block_from_packet
File: bin/iotlab-m3/ndn_encoding.a(shared_block.o)
Symbol table '.symtab' contains 197 entries:
   Num:    Value  Size Type    Bind   Vis      Ndx Name
     7: 00000001    28 FUNC    LOCAL  DEFAULT   56 atomic_set_to_one
   187: 00000001    84 FUNC    GLOBAL DEFAULT   58 ndn_shared_block_create
   192: 00000001    66 FUNC    GLOBAL DEFAULT   60 ndn_shared_block_create_b
   193: 00000001    56 FUNC    GLOBAL DEFAULT   62 ndn_shared_block_release
   196: 00000001    36 FUNC    GLOBAL DEFAULT   64 ndn_shared_block_copy
File: bin/iotlab-m3/ndn_encoding.a(metainfo.o)
Symbol table '.symtab' contains 194 entries:
   Num:    Value  Size Type    Bind   Vis      Ndx Name
   183: 00000001    68 FUNC    GLOBAL DEFAULT   57 ndn_metainfo_total_length
   186: 00000001   264 FUNC    GLOBAL DEFAULT   59 ndn_metainfo_wire_encode
   191: 00000001   180 FUNC    GLOBAL DEFAULT   61 ndn_metainfo_from_block
File: bin/iotlab-m3/ndn_encoding.a(data.o)
Symbol table '.symtab' contains 245 entries:
   Num:    Value  Size Type    Bind   Vis      Ndx Name
     7: 00000001     6 FUNC    LOCAL  DEFAULT   61 _finish_sha256
    10: 00000001     6 FUNC    LOCAL  DEFAULT   63 _update_sha256
    13: 00000001     6 FUNC    LOCAL  DEFAULT   65 _init_sha256
   216: 00000001   692 FUNC    GLOBAL DEFAULT   67 ndn_data_create
   232: 00000001   708 FUNC    GLOBAL DEFAULT   69 ndn_data_create2
   235: 00000001   100 FUNC    GLOBAL DEFAULT   71 ndn_data_get_name
   237: 00000001   108 FUNC    GLOBAL DEFAULT   73 ndn_data_get_metainfo
   239: 00000001   168 FUNC    GLOBAL DEFAULT   75 ndn_data_get_content
   240: 00000001   308 FUNC    GLOBAL DEFAULT   77 ndn_data_get_key_locator
   241: 00000001   502 FUNC    GLOBAL DEFAULT   79 ndn_data_verify_signature

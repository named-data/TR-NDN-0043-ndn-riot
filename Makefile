# change this to the file name of the paper 
PRJ = ndn-riot

LATEXRUN = .build/latexrun

all : ${PRJ}.pdf

${PRJ}.pdf : *.tex *.bib figures/*.pdf
	${LATEXRUN} ${PRJ}

view : ${PRJ}.pdf
	open ${PRJ}.pdf &

.PHONY: figure
figure:
	make -C figure

clean:
	rm -Rf latex.out
	rm -f *.toc *.aux ${PRJ}.bbl *.blg *.log ${PRJ}.ps ${PRJ}.pdf *.dvi *~* *.bak *.out *.gz


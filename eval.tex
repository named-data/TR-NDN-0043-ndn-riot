\section{Evaluation}
\label{sec:eval}

In this section we present the evaluation results we gathered from real IoT devices.
We focus on three aspects, memory usage, execution speed, and power consumption, all of which are critical for software systems running on constrained devices.
Our benchmark code is compiled with the GCC ARM Embedded toolchain on Ubuntu 16.04, which is based on GCC version 4.9.3.
We follow the default GCC settings from the RIOT-OS codebase, which uses level-2 optimization (\texttt{-O2}).

We perform the evaluation on two different IoT platforms:

\begin{itemize}
\item \textbf{SAMR21-XPRO}~\cite{samr21-xpro}: an IoT evaluation board produced by Atmel, which has a 32-bit ARM Cortex-M0+ 48 MHz microcontroller (MCU) with 32KB embedded RAM and 256KB embedded flash, and a 2.4 GHz IEEE 802.15.4 compliant radio interface.
\item \textbf{IoTLab-M3}~\cite{iotlab-m3}: an IoT board designed for the FIT IoTLab~\cite{iotlab}, which has a 32-bit ARM Cortex-M3 72 MHz MCU with 64KB embedded RAM and 512KB embedded flash, and the same type of radio interface as in SAMR21-XPRO.
\end{itemize}

\subsection{Memory Usage}

We measure the memory usage of the NDN-RIOT applications by inspecting the binary object code compiled for the two platforms.
We use the GNU \texttt{readelf} tool to obtain the code size of each API function, and the GNU \texttt{size} tool to obtain the total code size and the static memory usage from the executables of the skeleton consumer and producer examples shown in Section~\ref{sec:api}.

\begin{table}[!t]
% increase table row spacing, adjust to taste
\renewcommand{\arraystretch}{1.2}
% if using array.sty, it might be a good idea to tweak the value of
% \extrarowheight as needed to properly center the text within the cells
\caption{Object code size of the NDN-RIOT API (in bytes)}
\label{tab:api-size}
\centering
% Some packages, such as MDW tools, offer better commands for making tables
% than the plain LaTeX2e tabular which is used here.
\begin{tabular}{|l|c|c|}
\hline
\textbf{Function Name }& \textbf{ARMv6-M} & \textbf{ARMv7-M}\\
\hline
\scriptsize\texttt{ndn\_name\_from\_uri} & 420 & 408\\
\scriptsize\texttt{ndn\_name\_append} & 232 & 232\\
\scriptsize\texttt{ndn\_name\_get\_size\_from\_block} & 124 & 124\\
\scriptsize\texttt{ndn\_name\_get\_component\_from\_block} & 152 & 164\\
\hline
\scriptsize\texttt{ndn\_interest\_create} & 196 & 192\\
\scriptsize\texttt{ndn\_interest\_get\_name} & 92 & 94\\
\hline
\scriptsize\texttt{ndn\_data\_create} & 668 & 692\\
\scriptsize\texttt{ndn\_data\_get\_name} & 98 & 100\\
\scriptsize\texttt{ndn\_data\_get\_content} & 160 & 168\\
\scriptsize\texttt{ndn\_data\_verify\_signature} & 450 & 502\\
\hline
\scriptsize\texttt{ndn\_app\_run} & 612 & 596\\
\scriptsize\texttt{ndn\_app\_schedule} & 96 & 88\\
\scriptsize\texttt{ndn\_app\_express\_interest} & 160 & 168\\
\scriptsize\texttt{ndn\_app\_register\_prefix} & 180 & 180\\
\scriptsize\texttt{ndn\_app\_put\_data} & 60 & 56\\
\hline
\end{tabular}
\end{table}

Table~\ref{tab:api-size} lists the object code size of the core APIs in NDN-RIOT.
The second column shows the size of the APIs compiled for the ARM Cortex-M0+ MCU, which is based on the ARMv6-M Instruction Set Architecture (ISA).
The third column shows the size of the APIs compiled for the ARM Cortex-M3 MCU, which is based on the ARMv7-M ISA.
Both architectures support the Thumb instruction set with 16-bit/32-bit encoding, which leads to similar code sizes.

\begin{table}[!t]
% increase table row spacing, adjust to taste
\renewcommand{\arraystretch}{1.3}
% if using array.sty, it might be a good idea to tweak the value of
% \extrarowheight as needed to properly center the text within the cells
\caption{Overall memory usage of the skeleton NDN consumer and producer apps for RIOT-OS (in bytes)}
\label{tab:elf-size}
\centering
% Some packages, such as MDW tools, offer better commands for making tables
% than the plain LaTeX2e tabular which is used here.
\resizebox{\columnwidth}{!}{
\begin{tabular}{|c|c|r|r|r|r|r|}
\hline
\textbf{ISA} & \textbf{App} & \multicolumn{1}{c|}{\textbf{text}} & \multicolumn{1}{c|}{\textbf{data}} & \multicolumn{1}{c|}{\textbf{bss}} & \multicolumn{1}{c|}{\textbf{Flash}} & \multicolumn{1}{c|}{\textbf{RAM}} \\
\hline
ARMv6-M & Consumer & 35,300 & 192 & 11,208 & 35,492 & 11,400\\
\hline
ARMv7-M & Consumer & 33,900 & 192 & 11,208 & 34,092 & 11,400\\
\hline
ARMv6-M & Producer & 35,212 & 192 & 11,208 & 35,404 & 11,400\\
\hline
ARMv7-M & Producer & 33,800 & 192 & 11,208 & 33,992 & 11,400\\
\hline
\end{tabular}
}
\vspace{-0.5cm}
\end{table}

Table~\ref{tab:elf-size} shows the output from the GNU \texttt{size} command for the skeleton consumer and producer examples sketched out in Listings~\ref{list:consumer} and~\ref{list:producer}.
The source code of both examples have roughly the same structure, which therefore results in similar code sizes after compilation.
The last two columns in the table show the total amount of static memory residing in flash and RAM, respectively.
On a device with 32KB RAM, the static data occupies $\approx$11KB of the embedded RAM,%
\footnote{The static data in the .data and .bss sections includes fixed-size stack for each thread and other pre-allocated global objects.}
leaving 21KB for dynamic allocation, such as creating PIT, FIB and CS entries, creating shared memory blocks for NDN packets, and storing dynamic user data generated at run-time.

\subsection{Performance}

To gauge the run-time performance of the system, 
we first analyze the execution speed of individual APIs through a set of benchmarks, and then show the application-level RTT between two directly-connected nodes as an indicator for the packet processing speed of NDN-RIOT.
We did not measure the maximum network throughput since most IoT applications running on constrained devices do not require high throughput data transmission.

\subsubsection{API execution speed}
\label{sec:api-measure}

\begin{table}[!t]
% increase table row spacing, adjust to taste
\renewcommand{\arraystretch}{1.1}
% if using array.sty, it might be a good idea to tweak the value of
% \extrarowheight as needed to properly center the text within the cells
\caption{Execution time of the NDN-RIOT APIs}
\label{tab:api-time}
\centering
% Some packages, such as MDW tools, offer better commands for making tables
% than the plain LaTeX2e tabular which is used here.
\resizebox{\columnwidth}{!}{
\begin{tabular}{|c|r|r|r|r|}
\hline
\multirow{2}{*}{\textbf{Test Case}} & \multicolumn{2}{c|}{\textbf{SAMR21-XPRO}} & \multicolumn{2}{c|}{\textbf{IoTLab-M3}}\\
\cline{2-5}
 & \multicolumn{1}{c|}{\textbf{Time} ($\mu$s)} & \multicolumn{1}{c|}{\textbf{Cycles}} & \multicolumn{1}{c|}{\textbf{Time} ($\mu$s)} & \multicolumn{1}{c|}{\textbf{Cycles}} \\
\hline
URI to Name & 184 & 8,832 & 282 & 20,304\\
\hline
Get Name size & 13 & 624 & 11 & 792\\
\hline
Get Name component & 8 & 384 & 7 & 504\\
\hline
Append to Name & 28 & 1,344 & 29 & 2,088\\
\hline
Create Interest & 25 & 1,200 & 23 & 1,656\\
\hline
Get Interest Name & 2 & 96 & 2 & 144\\
\hline
Create Data (HMAC) & 1,806 & 86,688 & 1,333 & 95,976\\
\hline
Create Data (ECDSA) & 451,215 & 21,658,320 & 269,314 & 19,390,608\\
\hline
Verify Data (ECDSA) & 500,115 & 24,005,520 & 294,225 & 21,184,200\\
\hline
Get Data Name & 3 & 144 & 2 & 144\\
\hline
Get Data Content & 4 & 192 & 4 & 288\\
\hline
\end{tabular}
}
% \vspace{-0.5cm}
\end{table}

We measure the execution time of the NDN-RIOT APIs by calling the functions repeatedly and dividing the total running time by the number of iterations.
The results are presented in Table~\ref{tab:api-time} in both real time and MCU cycles for comparison across MCUs.
Since the benchmark suites run as a single-threaded application that takes over the whole MCU, the measurement results are quite stable over different runs.

% A surprising phenomenon to notice is that the IoTLab-M3 platform, which uses a faster MCU, performs significantly slower in terms of time and/or cycles in several test cases, compared to the SAMR21-XPRO platform.
% After some further analysis, we discovered that the load and store instructions on the IoTLab-M3 device actually take about 5 cycles to execute, while the memory access on the SAMR21-XPRO device usually finishes in 2 cycles, which matches the description in the ARM Cortex-M0+ Technical Reference Manual~\cite{cortex-m0p-manual} (Section 3.3).
% On the other hand, the IoTLab-M3 platform offers better performance on computation-intensive tasks such as creating and verifying Data packets.

To summarize, on SAMR21-XPRO, NDN-RIOT is able to create 5,434 NDN names (from URI strings), 40,000 Interests (using pre-generated name objects), or 553 HMAC-signed Data per second.
The most expensive operations are creating and verifying ECDSA-signed Data packets.
SAMR21-XPRO can create and verify $\approx$2 Data packets with ECDSA signatures per second.
IoTLab-M3 performs the same operation at 3.5--3.7 Data packets per second,%
\footnote{We noticed that the load/store instructions execute slower than expected on IoTLab-M3, causing several ``memory-bound'' test cases to run much slower than on SAMR21-XPRO.
The reason for the slow memory access is unclear.}
but it is still $\approx$150 times slower than using HMAC.

\subsubsection{Packet processing speed}

Our final evaluation measures the packet processing delay of the NDN-RIOT implementation by the application-level RTT of Interest-Data exchange between two directly-connected NDN-RIOT nodes.
The experiments are carried out on the FIT IoTLab testbed~\cite{iotlab} in Paris with two IoTLab-M3 nodes communicating directly with each other over IEEE 802.15.4 radio without interference from neighboring nodes.
The testbed network has an MTU of 102 bytes and a fixed data rate of 250~Kbps.

The measurement is performed under two scenarios with the Data packet size of 100 bytes and 196 bytes.
In each scenario, we measure the RTTs of \textit{fetching new Data generated by the producer upon request}, \textit{fetching pre-generated Data from the cache of the remote node (i.e., the producer)}, and \textit{fetching pre-generated Data from the consumer's local cache}.
Each experiment performs 100 Interest-Data exchange without pipelining and the RTT is calculated as the total running time divided by 100.
Since the experiments are executed in an isolated environment, there is no packet loss during the measurement.
All Data packets are signed by ECDSA.

\begin{table}[!t]
% increase table row spacing, adjust to taste
\renewcommand{\arraystretch}{1.1}
% if using array.sty, it might be a good idea to tweak the value of
% \extrarowheight as needed to properly center the text within the cells
\caption{Packet processing delay in Interest-Data exchange}
\label{tab:rtt}
\centering
% Some packages, such as MDW tools, offer better commands for making tables
% than the plain LaTeX2e tabular which is used here.
\begin{tabular}{|c|c|c|c|}
\hline
\textbf{Data Size} & \textbf{Cached?} & \textbf{Fragmented?} & \textbf{RTT} (ms)\\
\hline
\multirow{3}{*}{100 bytes}  & No & No & 280\\
\cline{2-4}
                            & Remote & No & 11 \\
\cline{2-4}
                            & Local & No & $<$1 \\
\hline
\multirow{3}{*}{196 bytes}  & No & Yes & 286 \\
\cline{2-4}
                            & Remote & Yes & 16 \\
\cline{2-4}
                            & Local & No & $<$1 \\
\hline
\end{tabular}
% \vspace{-0.5cm}
\end{table}

Table~\ref{tab:rtt} shows the RTT measurement results. 
When the consumer fetches newly created data, the RTT is dominated by the ECDSA signing delay (which takes $\approx$270~ms) for both packet sizes.
When the consumer fetches 196-byte Data, the packet is fragmented into two pieces at the producer and reassembled at the consumer node, and the RTT 
shows $\approx$6~ms additional delay compared to the results without fragmentation.
When the consumer fetches data from its local cache, no fragmentation is required for either packet size and the response time is less than 1ms.

The average RTT of IP packets between two RIOT-OS devices in the testbed environment ranges from 5 to 9~ms as measured by the \texttt{ping} utility in the RIOT-OS kernel.
Thus, the Interest-Data RTT of NDN-RIOT, which is yet to be fine tuned, is comparable to that of the IP stack (without the data signing delay).
%\footnote{Our experiments in the same environment with the ping utility from RIOT-OS kernel showed RTT for IP-based communication from 5 to 10~ms.}
However, we note that the former can express a higher level semantics and directly feed data to applications, while an IP packet must go through additional layers of protocol processing, plus extra round trips for naming resolution and secured session setup, before reaching the application layer as explained in~\cite{shang2016challenges}.

\subsection{Power Usage}

We measure the power usage of the NDN-RIOT APIs using the IoTLab-M3 node, which is running on a 3.7V LiPo battery with 650 mAh capacity\footnote{\url{https://www.iot-lab.info/hardware/m3/}}.
The IoTLab testbed~\cite{iotlab} conveniently provides the profiling tools for the users to collect the power consumption data at certain sampling rate from the testbed nodes.
We run the same test suite used in Section~\ref{sec:api-measure} but increase the running time of each test case to 30 seconds in order to minimize the noise in the power data sampling.
We first measure the baseline power consumption of the M3 node when it is idle, which is about 0.14 W.
The power usage of each test case is then calculated by subtracting the baseline power from the power when the test case is running.
The average running time of each API is calculated as the total run-time divided by the number of repeated runs.
Finally, the energy consumption is calculated as power times the average run-time.

\begin{table}[!t]
% increase table row spacing, adjust to taste
% \renewcommand{\arraystretch}{1.1}
% if using array.sty, it might be a good idea to tweak the value of
% \extrarowheight as needed to properly center the text within the cells
\caption{Power usage of the NDN-RIOT APIs}
\label{tab:api-power}
\centering
% Some packages, such as MDW tools, offer better commands for making tables
% than the plain LaTeX2e tabular which is used here.
% \resizebox{\columnwidth}{!}{
\begin{tabular}{|c|r|r|r|}
\hline
\textbf{Test Case} & \textbf{Power} (mW) & \textbf{Time} ($\mu$s) & \textbf{Energy} (nJ)\\
\hline
URI to Name & 5 & 273 & 1,365\\
\hline
Get Name size & 7.5 & 12 & 90\\
\hline
Get Name component & 8.5 & 8 & 68\\
\hline
Append to Name & 8.5 & 30 & 255\\
\hline
Create Interest & 9.5 & 23 & 218.5\\
\hline
Get Interest Name & 9.5 & 2 & 19\\
\hline
Create Data (HMAC) & 12 & 1,333 & 15,996\\
\hline
Create Data (ECDSA) & 17 & 269,325 & 4,578,525\\
\hline
Verify Data (ECDSA) & 17 & 294,237 & 5,002,029\\
\hline
Get Data Name & 11 & 3 & 33\\
\hline
Get Data Content & 9.5 & 4 & 38\\
\hline
\end{tabular}
% }
% \vspace{-0.5cm}
\end{table}

The measurement results are listed in Table~\ref{tab:api-power}.%
\footnote{The results of some test cases are sightly different from those in Table~\ref{tab:api-time} because they were collected in different runs.}
% It is not surprising to see that signing and verifying data packets with ECDSA are the most energy-consuming operations.
% It is also interesting to see that the ``memory-bound'' test cases (such as converting URI to encoded NDN name) consume less power than the ``CPU-bound'' test cases (such as generating Data packets with HMAC and ECDSA).
To put these numbers into perspective, consider a simple sensing application running on M3 that wakes up and reads the sensor measurement every 1 minute, packages the data point in an NDN data packet signed by ECDSA, immediately transmits the packet over IEEE 802.15.4 interface, then goes back to sleep.
The Data packet generation will take about 300 ms using 0.157 W of power (including the baseline), which results in a total of 47.1 mJ.
The IEEE 802.15.4 transceiver on the M3 node running at maximum transmit power consumes 14 mA at 3.6 V by itself,%
\footnote{\url{http://www.atmel.com/images/doc8111.pdf}}
and takes less than 4 ms to transmit a full-MTU-size packet, which costs a total energy of about 0.2 mJ.
With the 650 mAh battery, the M3 node could (in theory) run the application for 127 $(=650 * 3.7 * 3600 / 47.3 / 60 / 24)$ days without recharging.
Note that this estimation does not cover the energy consumed by the sensor motes or during the transition between hibernation and wake-up.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "ndn-riot"
%%% End:
